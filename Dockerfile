# Python Image
FROM python:alpine3.6
WORKDIR /app

 # Add script to container
ADD ./src/sync.py /app

# Copy custom pip modules
COPY ./pip/requirements.txt ./custom.txt

# Install Requirements
RUN apk add --no-cache tini \
    git openssh \
    build-base \
    zlib-dev && \
  pip install -r custom.txt && \
  apk del build-base
