# sync-bot

A git bot that can sync specific files between two repos. Designed to run as part of GitLab's CI/CD system.