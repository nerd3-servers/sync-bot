from git import Repo
from shutil import copy, rmtree
import os
import json
from urllib.parse import urlparse
import gitlab
import time

def get_last_commit(repo, file):
    r = repo.git.log(
        '-1', '--date=unix',
        "--pretty=format:'{\"date\": %ad, \"author\": \"%an\", \"email\": \"%ae\", \"commit\": \"%h\", \"message\": \"%s\"}'",
        file).strip("\'")
    return json.loads(r)

u = os.environ.get('SOURCE_REPO')
if not u:
    u = os.environ['CI_REPOSITORY_URL']
path = urlparse(u).path[1:][:-4]

print('Cloning source...')
source = Repo.clone_from(u, 'source')
print('Cloning target...')
target = Repo.clone_from(
    os.environ['TARGET_REPO'].replace(
        'https://',
        f'https://{os.environ["GIT_USERNAME"]}:{os.environ["GIT_PASSWORD"]}@'),
    'target')
target.config_writer().set_value("user", "name",
                                 os.environ["GIT_NAME"]).release()
target.config_writer().set_value("user", "email",
                                 os.environ["GIT_EMAIL"]).release()

source_location = f'{os.getcwd()}/source'
target_location = f'{os.getcwd()}/target'

forked = False
changes = 0

print('Sycning files...')
to_commit = False
with open(f'source/{os.environ["FILE_PATH"]}') as file:
    for line in file.read().splitlines():
        print(f'Checking {line}...')
        os.makedirs(os.path.dirname(f'{target_location}/{line}'), exist_ok=True)
        source_c = get_last_commit(source, line)
        new_file = False
        try:
            target_c = get_last_commit(target, line)
            if source_c['date'] < target_c['date']:
                # Target commit is newer, so ignore it
                continue
            if (source_c['author'],
                    source_c['email']) == (os.environ["GIT_NAME"],
                                           os.environ["GIT_EMAIL"]):
                # Commit was probably made by the bot itself, so ignore it
                continue
        except:
            new_file = True
        copy(f'{source_location}/{line}', f'{target_location}/{line}')
        if not new_file:
            changed = [item.a_path for item in target.index.diff(None)]
            if line not in changed:
                # File is the same, so no point in commiting
                continue
        if not forked and int(os.environ["MERGE_REQUEST"]) == 1:
            forked = True
            branch_name = f'merge-from-{path}-{int(time.time())}'
            print(f'Creating new branch: {branch_name}...')
            h = target.create_head(branch_name)
            target.head.set_reference(h)
        target.index.add([line])
        print(f'Committing {line}...')
        target.index.commit(
            f'Commit {source_c["commit"]} from {path} by {source_c["author"]}: {source_c["message"]}'
        )
        to_commit = True
        changes += 1

if to_commit:
    print('Pushing changes...')
    if forked:
        target.git.push('--set-upstream', 'origin', branch_name)
        p_url = urlparse(os.environ['TARGET_REPO'])
        gl = gitlab.Gitlab(f'{p_url.scheme}://{p_url.netloc}/', private_token=os.environ["GIT_PASSWORD"])
        project = gl.projects.get(p_url.path[1:].replace('.git', ''))
        project.mergerequests.create({
            'source_branch': branch_name,
            'target_branch': 'master',
            'title': f'Merge {changes} changed file{"s" if changes != 1 else ""} from {path}',
            'remove_source_branch': True,
            'allow_collaboration': True
        })
    else:
        target.remotes.origin.push()
    print('Done!')
else:
    print('Nothing to push')
